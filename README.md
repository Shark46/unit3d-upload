# unit3d-upload

## Features:
* hashes the file/folder (optional - will not hash if it finds a .torrent with the same name as the file/folder you're uploading)
* generates the mediainfo
* generates screenshots and uploads them to imgbb (optional)
* prompts uploader for various info needed to upload
* uploads via the unit3d upload API
* copies torrent file to watch directory (optional - will skip this if you leave the "watch_dir" variable blank)

## Requirements:
* python3
* requests library for python3
* dottorrent library for python3
* mediainfo
* ffmpeg (optional - needed for screenshot generation)

## Installation:

* install the requirements
* copy the script into a folder that's in your PATH so you can run the script from anywhere and make it executable
* open the script with a text editor and edit the variables at the top of the script in the user config sections

## Usage:

Flags:
* ` -m    path to file to use for mediainfo`
* ` -c    category number (found in the cats array, eg. -c 1)`
* ` -t    torrent type (found in types array, eg. -t 1080p)`
* ` -x    exclude files from hashing using globs (eg. -x *.jpg *.torrent)`

To upload a single file run the following command:

`unit3d-upload my.video.file.mkv`

To upload a folder: (the -m flag is required)

`unit3d-upload my.folder -m my.folder/my.video.file.mkv`

The script will then prompt you for info about the upload, such as the torrent name, description, category, type, etc...
In order to use to displayed default value, just press enter.
